
const path = require('path');
const program = require('commander');

const { logError, logInfo } = require('./scripts/log');

const pipelineApi = require('./scripts/pipeline');

const API_URL = 'https://gitlab.com/api/v4';

program
  .version('1.0.0')
  .option('-t, --token <token>', 'GitLab private token (Required)')
  .option('-pi, --project-id <project-id>', 'Project ID of target project (Required)')
  .option('-pp, --per-page <per-page>', 'Number of pipelines to process at a time (Optional, defaults to 20)')
  .parse(process.argv);

const TOKEN = program.token;
const PROJECT_ID = program.projectId;
const PER_PAGE = program.perPage;

const apiConfig = { apiUrl: API_URL, token: TOKEN };

pipelineApi.getPipelines(apiConfig, PROJECT_ID, PER_PAGE)
  .then(({ data }) => {
    data.forEach((pipeline) => {
      const threshold = new Date();
      threshold.setDate(threshold.getDate()-90);

      const shouldDelete = new Date(pipeline.created_at) < threshold;

      if (shouldDelete) {
        logInfo(`Deleting pipeline ${pipeline.web_url} (${pipeline.id})`)
        pipelineApi.deletePipeline(apiConfig, PROJECT_ID, pipeline.id)
      }
    });
  })
  .catch(error => logError(error));
