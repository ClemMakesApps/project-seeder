const axios = require('axios');

function runPipeline({ apiUrl, token}, projectId, branchName) {
  return axios.post(`${apiUrl}/projects/${projectId}/pipeline?ref=${branchName}`, {}, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  });
}

function getPipelines({ apiUrl, token }, projectId, perPage = 20) {
  return axios.get(`${apiUrl}/projects/${projectId}/pipelines?sort=asc&per_page=${perPage}`, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  })
}

function deletePipeline({ apiUrl, token}, projectId, pipelineId) {
  return axios.delete(`${apiUrl}/projects/${projectId}/pipelines/${pipelineId}`, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  })
}

module.exports = {
  runPipeline,
  getPipelines,
  deletePipeline
};
