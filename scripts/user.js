const axios = require('axios');

function getUserId({ apiUrl }, username) {
  return axios.get(`${apiUrl}/users?username=${username}`)
    .then(({ data }) => {
      return data[0].id;
    })
}

function addUserToProject({ apiUrl, token }, userId, newProjectId, expiresAt = null, accessLevel = 30) {
  // Access levels
  // 10 => Guest access
  // 20 => Reporter access
  // 30 => Developer access
  // 40 => Maintainer access
  // 50 => Owner access # Only valid for groups

  const data = {
    user_id: userId,
    access_level: accessLevel,
  };

  if (expiresAt) {
    data.expires_at = expiresAt;
  }

  return axios.post(`${apiUrl}/projects/${newProjectId}/members`, data, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  });
}

module.exports = {
  getUserId,
  addUserToProject,
}
